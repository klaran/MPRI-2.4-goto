open Fold
open Mu

module CStack = struct
  type 'a f =
    | Empty
    | Cons of int * 'a

  let map f s =
    match s with
    | Empty -> Empty
    | Cons (x, s) -> Cons (x, f s)
end

module Stack = Mu (CStack)

let empty () = Stack.Constr CStack.Empty

let push n ns = Stack.Constr (CStack.Cons (n, ns))

let rec from_stack = function
  | Spec.Empty -> empty ()
  | Spec.Push (n, ns) -> push n (from_stack ns)
