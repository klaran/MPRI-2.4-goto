open Fold
open Mu

module CRose = struct
  type ('a, 'b) f1 = RNode of int * 'b

  type ('a, 'b) f2 =
    | RNil
    | RCons of 'a * 'b

  let map1 _ f2 (RNode (n, rs)) = RNode (n, f2 rs)

  let map2 f1 f2 rs =
    match rs with
    | RNil -> RNil
    | RCons (r, rs) -> RCons (f1 r, f2 rs)
end

module Rose = Mu2 (CRose)

let node n ts = Rose.RConstr1 (RNode (n, ts))

let nil () = Rose.RConstr2 (RNil)

let cons t ts = Rose.RConstr2 (RCons (t, ts))

let rec from_rose = function
    | Spec.RNode (n, rs) -> node n (from_roses rs)

and from_roses = function
    | Spec.RNil -> nil ()
    | Spec.RCons (r, rs) -> cons (from_rose r) (from_roses rs)
