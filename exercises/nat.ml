open Fold
open Mu

module CNat = struct
  type 'a f =
    | Zero
    | Succ of 'a

  let map f n =
    match n with
    | Zero -> Zero
    | Succ n -> Succ (f n)
end

module Nat = Mu (CNat)

let zero () = Nat.Constr Zero

let succ n = Nat.Constr (Succ n)

let rec from_nat = function
  | Spec.Zero -> zero ()
  | Spec.Succ n -> succ (from_nat n)
