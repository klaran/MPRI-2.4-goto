open Fold
open Stack

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgCat = struct
  module F = CStack

  type carrier = Stack.t

  type ctxt = Stack.t

  let alg (type x) (f : x * Stack.t -> carrier) = function
    | CStack.Empty, s -> s
    | CStack.Cons (m, ms), s -> push m (f (ms, s))
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)

module Cat = Curry.Fix(AlgCat)

let cat ms ns = Cat.fix (ms, ns)

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x1 = Spec.Empty in
  let x2 = Spec.Empty in
  from_stack (Spec.cat x1 x2) = cat (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Push (42, Spec.Empty) in
  let x2 = Spec.Empty in
  from_stack (Spec.cat x1 x2) = cat (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Empty in
  let x2 = Spec.Push (42, Spec.Empty) in
  from_stack (Spec.cat x1 x2) = cat (from_stack x1) (from_stack x2)

let%test _ =
  let x1 = Spec.Push (1, Spec.Push (2, Spec.Push (3, Spec.Empty))) in
  let x2 = Spec.Push (42, Spec.Empty) in
  from_stack (Spec.cat x1 x2) = cat (from_stack x1) (from_stack x2)
