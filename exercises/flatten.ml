open Fold
open Rose

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgFlatten = struct
  module F = CRose

  type carrier1 = int list

  type carrier2 = int list

  let alg1 _ _flattens = function
    | CRose.RNode (n, rs) -> n :: (_flattens rs)

  let alg2 _flattena _flattens = function
    | CRose.RNil -> []
    | CRose.RCons (r, rs) -> (_flattena r) @ (_flattens rs)
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)

module Flatten = Mutual.Fix(AlgFlatten)

let flattena r = Flatten.fix1 r

let flattens rs = Flatten.fix2 rs

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.RNode (0, Spec.RNil) in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x = Spec.RNode (0, Spec.RCons (Spec.RNode (3, Spec.RNil), Spec.RNil)) in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x =
    Spec.RNode
      ( 0
      , Spec.RCons
          ( Spec.RNode (3, Spec.RNil)
          , Spec.RCons (Spec.RNode (2, Spec.RNil), Spec.RNil) ) )
  in
  Spec.flattena x = flattena (from_rose x)

let%test _ =
  let x =
    Spec.RNode
      ( 0
      , Spec.RCons
          ( Spec.RNode (3, Spec.RCons (Spec.RNode (1, Spec.RNil), Spec.RNil))
          , Spec.RCons (Spec.RNode (2, Spec.RNil), Spec.RNil) ) )
  in
  Spec.flattena x = flattena (from_rose x)
