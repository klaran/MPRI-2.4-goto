open Algebra
open Mu

(*****************************************************************)
(* Generic recursor                                              *)
(*****************************************************************)

module Fix (A : Alg2) = struct
  module MuF = Mu2 (A.F)
  open MuF

  let rec fix1 (RConstr1 f) = A.alg1 (fun x -> fix1 x) (fun y -> fix2 y) f

  and fix2 (RConstr2 f) = A.alg2 (fun x -> fix1 x) (fun y -> fix2 y) f
end
