open Algebra
open Mu

(*****************************************************************)
(* Generic recursor                                              *)
(*****************************************************************)

module Fix (A : AlgCurry) = struct
  module MuF = Mu (A.F)
  open MuF

  let rec fix (Constr t, ctx) = A.alg (fun x -> fix x) (t, ctx)
end
