open Algebra
open Mu

(*****************************************************************)
(* Generic recursor                                              *)
(*****************************************************************)

module Fix (A : AlgPara) = struct
  module MuF = Mu (A.F)
  open MuF

  let rec fix1 (Constr t) = A.alg1 (fun x -> fix1 x) (fun x -> fix2 x) t

  and fix2 (Constr t) = A.alg2 (fun x -> fix1 x) (fun x -> fix2 x) t
end

(* Section 5.5 *)
