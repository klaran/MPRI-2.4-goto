open Algebra
open Mu

(*****************************************************************)
(* Generic recursor                                              *)
(*****************************************************************)

module Fix (A : AlgCata) = struct
  module MuF = Mu (A.F)
  open MuF

  let rec fix (Constr t) = A.alg (fun x -> fix x) t
end
